# TRP Testing

Travel Rule Protocol (TRP) testing tools

## Description

The following tests are implemented following the "Conformance Test Cases" defined here: https://docs.google.com/spreadsheets/d/1X4TlWT5OF13b2h69nR_hgFPRq5EPR0Pji5-n-AkBFBY

## How to run

All the tests are developed as a Postman Collection.

There is only one enviroment variable: `baseURL`

To run the test, the collections file can be loaded into Postman and run there.

Also the collection can be run without Postman by using newman

```
$ npx newman run trp-test.postman_collection.json --global-var baseUrl=$baseURL
```

## Testing known implementations

_If you are running a TRP server that support this tests please submit a Merge Request to add it here._

<<<<<<< HEAD
=======
### 21 Analytics
```
$ npx newman run trp-test.postman_collection.json --global-var baseUrl=http://testing.21analytics.ch:3030/
```

>>>>>>> 40fedf567199c8ca7de4adc0922deb0c4ffa8731
### Notabene
```
$ npx newman run trp-test.postman_collection.json --global-var baseUrl=https://trptest.notabene.id/trp
```
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/d16f72e3a187841c207f#?env%5Btrp-testservice%5D=W3sia2V5IjoiYmFzZVVybCIsInZhbHVlIjoiaHR0cHM6Ly90cnB0ZXN0Lm5vdGFiZW5lLmlkL3RycCIsImVuYWJsZWQiOnRydWV9XQ==)

